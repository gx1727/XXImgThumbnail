**php图片缩略图工具类**


基于php的图片处理库，参照网上找到相关代码，整理出该class，方便以后调用。


```
//创建对象，以原始图片url为参数
//$img_url:原始图片url
$img = new XXImgThumbnail($img_url);
$img->set_bkcolor(255, 255, 255); //设置旁白颜色
$img->set_quality(80);　//设置图片质量
$img->set_mod($mod);　//设置缩略图方案
$img->set_max_size($max_width, $max_height);//设置缩略图　长　宽
$img->show();　// 直接显示缩略图

//保存缩略图
//$save_url 保存缩略图地址
//$img->save($save_url);
```
     
注：最终生成的图片都为jpg格式
演示地址:http://gx1727.com/simple/other/XXImgThumbnail
