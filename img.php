<?php
/**
 * Created by PhpStorm.
 * User: gx1727
 * Date: 2016-01-15
 * Time: 15:17
 */
//http://localhost-e/img.php?/uploads/151119/1_200_200.jpg
//调整图片大小
/**
 *图片按比例调整大小的原理：
 *1、比较原图大小是否小于等于目标大小，如果是则直接采用原图宽高
 *2、如果原图大小超过目标大小，则对比原图宽高大小
 *3、如：宽>高，则宽=目标宽, 高=目标宽的比例 * 原高
 *4、如：高>宽，则高=目标高，宽=目标高的比例 * 原宽
 **/

include("XXImgThumbnail.php");

function get_image_src($mime, $image)
{
    if ($mime == "image/jpeg") {
        return imagecreatefromjpeg($image);
    } else if ($mime == "image/png") {
        return imagecreatefrompng($image);
    } else if ($mime == "image/gif") {
        return imagecreatefromgif($image);
    }
}

$base_path = ""; //根地址
$default = ""; //默认图
$max_width = 200;
$max_height = 200;
$mod = 1;
$thumb = true; //要缩略图
$uri = explode('?', $_SERVER['REQUEST_URI']);
if ($uri[1][0] == '/') {
    $img_url = substr($uri[1], 1);
} else {
    $img_url = $uri[1];
}

$param_arr = explode('/', $img_url);
if (preg_match('/^(\d+)_(\d+)_(\d+)$/i', $param_arr[0], $arr)) {
    $max_width = $arr[1];
    $max_height = $arr[2];
    $mod = $arr[3];
    $img_url = substr($img_url, strlen($param_arr[0]) + 1);
} else {
    $thumb = false; //显示原图
}

//$img_url_arr = explode('uploads', $img_url);
//$image = 'uploads/' . $img_url_arr[1];

$img = new XXImgThumbnail($img_url);
$img->set_bkcolor(255, 255, 255);
$img->set_quality(80);
$img->set_mod($mod);
$img->set_max_size($max_width, $max_height);
$img->show();